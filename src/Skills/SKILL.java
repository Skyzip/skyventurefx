package Skills;

public enum SKILL {
    armorProficiency,
    swordProficiency, maceProficiency, axeProficiency,
    implant,
    weaponFocus,
    dueling,
    twoWeaponMastery,
    toughness,
    conditioning,
    magicAffinity,
    magicResistance,
    reflex,
    regen,

    powerAttack, criticalStrike, flurry,
    protect, absorb,
    lightning, drain, mayhem,
    rage,
    stealth
}

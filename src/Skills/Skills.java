package Skills;

import java.util.HashMap;

// Kind of a structure
public class Skills {

    public HashMap<SKILL, Skill> skillMap = new HashMap<>();
    public PassiveSkills passiveSkills = new PassiveSkills();
    public ActiveSkills activeSkills = new ActiveSkills();

    public Skills() {}
    public Skills(SkillType[] skillTypes) {

        for (SkillType skillType : skillTypes) {
            this.skillMap.get(skillType.skillType).point = skillType.point;
        }
    }


    // Skill data type
    static public class Skill {
        public int point;
        public String description;
        public int cost;

        public Skill(int point, String description, int cost) {
            this.point = point;
            this.description = description;
            this.cost = cost;
        }
    }

    public class PassiveSkills {

        public HashMap<SKILL, Skill> skillMap = new HashMap<>();

        private PassiveSkills() {

            this.skillMap.put(SKILL.armorProficiency, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.swordProficiency, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.maceProficiency, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.axeProficiency, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.implant, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.weaponFocus, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.dueling, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.twoWeaponMastery, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.toughness, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.conditioning, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.magicAffinity, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.magicResistance, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.reflex, new Skill(0, "non", 0));
            this.skillMap.put(SKILL.regen, new Skill(0, "non", 0));

            Skills.this.skillMap.putAll(this.skillMap);
        }
    }

    public class ActiveSkills {

        public HashMap<SKILL, Skill> skillMap = new HashMap<>();

        private ActiveSkills() {
            this.skillMap.put(SKILL.powerAttack, new Skill(0, "non", 20));
            this.skillMap.put(SKILL.criticalStrike, new Skill(0, "non", 20));
            this.skillMap.put(SKILL.flurry, new Skill(0, "non", 20));
            this.skillMap.put(SKILL.protect, new Skill(0, "non", 120));
            this.skillMap.put(SKILL.absorb, new Skill(0, "non", 90));
            this.skillMap.put(SKILL.lightning, new Skill(0, "non", 50));
            this.skillMap.put(SKILL.drain, new Skill(0, "non", 60));
            this.skillMap.put(SKILL.mayhem, new Skill(0, "non", 70));
            this.skillMap.put(SKILL.rage, new Skill(0, "non", 150));
            this.skillMap.put(SKILL.stealth, new Skill(0, "non", 15));

            Skills.this.skillMap.putAll(this.skillMap);
        }
    }
}

package Items.Armor;

public abstract class BodyArmor extends Armor {

    public BodyArmor(String name, int level, int defense, int magicDefense, int maxDurability, int tier) {
        super(name, level, defense, magicDefense, maxDurability, tier);
    }
}
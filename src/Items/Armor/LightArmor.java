package Items.Armor;

public class LightArmor extends BodyArmor {

    public LightArmor(String name, int level, int defense, int magicDefense, int maxDurability, int tier) {
        super(name, level, defense, magicDefense, maxDurability, tier);
    }
}
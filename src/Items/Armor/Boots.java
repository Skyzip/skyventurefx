package Items.Armor;

public class Boots extends Armor {

    public Boots(String name, int level, int defense, int magicDefense, int maxDurability, int tier) {
        super(name, level, defense, magicDefense, maxDurability, tier);
    }
}
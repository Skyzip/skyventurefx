package Items.Armor;

public class Helmet extends Armor {

    public Helmet(String name, int level, int defense, int magicDefense, int maxDurability, int tier) {
        super(name, level, defense, magicDefense, maxDurability, tier);
    }
}
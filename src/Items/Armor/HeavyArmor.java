package Items.Armor;

public class HeavyArmor extends BodyArmor {

    public HeavyArmor(String name, int level, int defense, int magicDefense, int maxDurability, int tier) {
        super(name, level, defense, magicDefense, maxDurability, tier);
    }
}
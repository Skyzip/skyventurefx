package Items.Armor;

public class Gloves extends Armor {

    public Gloves(String name, int level, int defense, int magicDefense, int maxDurability, int tier) {
        super(name, level, defense, magicDefense, maxDurability, tier);
    }
}
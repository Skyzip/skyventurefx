package Items.Armor;

import Items.Upgradeable;

public abstract class Armor extends Upgradeable {

    private String name;

    private int durability;
    private int maxDurability;
    private int defense;
    private int magicDefense;
    private int tier;

    public Armor() {}

    public Armor(String name, int level, int defense, int magicDefense, int maxDurability, int tier) {
        this.name = name;
        this.defense = defense;
        this.magicDefense = magicDefense;
        this.durability = maxDurability;
        this.maxDurability = maxDurability;
        this.tier = tier;

        this.setLevel(level);
    }

    @Override
    public boolean LvlUp() {
        if (this.upgradeable)
        {
            this.level++;
            this.defense += (int)((double)this.defense*(1./10.));
            this.magicDefense = this.magicDefense + (int)(this.magicDefense*((double)this.level/10.));

            if (this.level >= 10)
                this.upgradeable = false;

            return true;
        }

        return false;
    }
}

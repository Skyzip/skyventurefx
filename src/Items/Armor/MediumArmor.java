package Items.Armor;

public class MediumArmor extends BodyArmor {

    public MediumArmor(String name, int level, int defense, int magicDefense, int maxDurability, int tier) {
        super(name, level, defense, magicDefense, maxDurability, tier);
    }
}
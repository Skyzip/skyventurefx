package Items;

public abstract class Upgradeable extends Item {
    protected int level = 1;
    protected boolean upgradeable = true;

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        for (int i = this.level; i < level; i++)
        {
            this.LvlUp();
        }
    }

    public boolean isUpgradeable() {
        return this.upgradeable;
    }
    
    // will be overridden
    public boolean LvlUp() {return false;}
}
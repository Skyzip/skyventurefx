package Items.Consumable;

import Items.Item;

public abstract class Consumable extends Item {
}

class Potion extends Consumable {
    private int points;

    public Potion(int points) {
        this.points = points;
    }

    public int getPoints() {
        return this.points;
    }
}





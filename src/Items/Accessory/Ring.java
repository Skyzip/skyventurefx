package Items.Accessory;

import Skills.*;

public class Ring extends Accessory {

    public Ring() {
        bonusSkills = new Skills();
    }

    public Ring(SkillType[] skillTypes) {

        bonusSkills = new Skills(skillTypes);
    }
}
package Items.Accessory;

import Skills.*;

public class Implant extends Accessory {

    public Implant() {
        bonusSkills = new Skills();
    }

    public Implant(SkillType[] skillTypes) {

        bonusSkills = new Skills(skillTypes);
    }
}
package Items.Accessory;

import Skills.*;

public class Belt extends Accessory {

    public Belt() {
        bonusSkills = new Skills();
    }

    public Belt(SkillType[] skillTypes) {

        bonusSkills = new Skills(skillTypes);
    }
}
package Items.Accessory;

import Items.Item;
import Skills.*;

public abstract class Accessory extends Item {

    protected Skills bonusSkills;

    public Accessory() {
        bonusSkills = new Skills();
    }

    public Accessory(SkillType[] skillTypes) {

        bonusSkills = new Skills(skillTypes);
    }







}
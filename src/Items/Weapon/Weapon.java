package Items.Weapon;

import Items.Upgradeable;

public abstract class Weapon extends Upgradeable {

    private String name;

    private int damage;
    private int magicDamage;
    private int durability;
    private int maxDurability;
    private int tier;
    
    public Weapon(String name, int level, int damage, int magicDamage, int maxDurability, int tier) {
        this.name = name;
        this.damage = damage;
        this.magicDamage = magicDamage;
        this.durability = maxDurability;
        this.maxDurability = maxDurability;
        this.tier = tier;

        super.setLevel(level);
    }

    public int getDamage() {
        return this.damage;
    }

    public int getDurability() {
        return this.durability;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }

    @Override
    public boolean LvlUp() {
        if (super.isUpgradeable())
        {
            this.level++;
            this.damage += (int)((double)this.damage*(1./10.));
            this.magicDamage = this.magicDamage + (int)(this.magicDamage*((double)this.level/10.));

            if (this.level >= 10)
                this.upgradeable = false;
                
            return true;
        }
        
        return false;
    }
}





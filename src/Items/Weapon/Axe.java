package Items.Weapon;

public class Axe extends Weapon {

    private int plusDamage = 10;

    public Axe(String name, int level, int damage, int magicDamage, int maxDurability, int tier) {
        super(name, level, damage, magicDamage, maxDurability, tier);
    }

    @Override
    public boolean LvlUp() {
        if (super.LvlUp())
        {
            this.plusDamage = (int)((double)this.getDamage()*((double)this.level/10.));

            return true;
        }

        return false;
    }
}
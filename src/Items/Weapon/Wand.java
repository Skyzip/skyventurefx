package Items.Weapon;

public class Wand extends Weapon {

    private int plusMagicAffinity = 0;
    private int plusMP = 0;

    public Wand(String name, int level, int damage, int magicDamage, int maxDurability, int tier) {
        super(name, level, damage, magicDamage, maxDurability, tier);
    }

    @Override
    public boolean LvlUp() {
        if (super.LvlUp())
        {
            if (this.level % 3 == 0 || this.level % 10 == 0)
                this.plusMagicAffinity++;

            plusMP += 20;

            return true;
        }

        return false;
    }
}
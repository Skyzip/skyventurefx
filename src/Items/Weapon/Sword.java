package Items.Weapon;

public class Sword extends Weapon {

    private int criticalChance = 0;
    private int criticalDamage = 10;

    public Sword(String name, int level, int damage, int magicDamage, int maxDurability, int tier) {
        super(name, level, damage, magicDamage, maxDurability, tier);
    }

    @Override
    public boolean LvlUp() {
        if (super.LvlUp())
        {
            if (this.level % 2 == 0)
            {
                this.criticalDamage *= 1.8;
                this.criticalChance += 1;
            }

            this.criticalChance += 2;

            return true;
        }

        return false;
    }
}
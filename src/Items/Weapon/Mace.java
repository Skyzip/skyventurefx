package Items.Weapon;

public class Mace extends Weapon {

    private int ignoresArmor = 0;

    public Mace(String name, int level, int damage, int magicDamage, int maxDurability, int tier) {
        super(name, level, damage, magicDamage, maxDurability, tier);
    }

    @Override
    public boolean LvlUp() {
        if (super.LvlUp())
        {
            if (this.level % 2 == 0)
            {
                this.ignoresArmor += 5;
            }
            else
            {
                this.ignoresArmor *= 1.5;
            }

            return true;
        }

        return false;
    }
}
package GameCharacters.Player;

import Items.*;
import Items.Weapon.*;
import Items.Armor.*;
import Items.Accessory.*;
import Skills.*;

import java.util.ArrayList;
import java.util.Random;

public abstract class GameCharacter {

    private int level = 1;

    private int HP;
    private int MP;
    private int STR;
    private int DEF;
    private int INT;
    private int DEX;

    private Weapon handLeft = null;
    private Weapon handRight = null;

    private BodyArmor body = null;
    private Boots legs = null;
    private Helmet head = null;
    private Gloves arms = null;
    private Ring fingers = null;
    private Belt waist = null;
    private Implant implant = null;

    private ArrayList<Item> storage = new ArrayList<Item>();

    private Skills skills = new Skills();


    protected int GetCarryWeight() {
        return 10*this.STR;
    }

    protected double GetMasteryBonus(int mastery) {

        if (mastery <= 0)
            return 0;

        return 0.8 * Math.pow(1.25, mastery - 1);
    }

    protected void TakeDamage(int damage) {
        this.HP -= damage;
    }
    
    protected void TakeDamage(GameCharacter from) {
    }

    protected int GenDmg() {
        Random r = new Random();

        int dmgL = this.handLeft == null ? 0:this.handLeft.getDamage();
        if (this.handLeft instanceof Sword)
            dmgL = (int)((double)dmgL * GetMasteryBonus(this.skills.skillMap.get(SKILL.swordProficiency).point));
        else if (this.handLeft instanceof Mace)
            dmgL = (int)((double)dmgL * GetMasteryBonus(this.skills.skillMap.get(SKILL.maceProficiency).point));
        else if (this.handLeft instanceof Axe)
            dmgL = (int)((double)dmgL * GetMasteryBonus(this.skills.skillMap.get(SKILL.axeProficiency).point));

        int dmgR = this.handRight == null ? 0:this.handRight.getDamage();
        if (this.handRight instanceof Sword)
            dmgR = (int)((double)dmgR * GetMasteryBonus(this.skills.skillMap.get(SKILL.swordProficiency).point));
        else if (this.handRight instanceof Mace)
            dmgR = (int)((double)dmgR * GetMasteryBonus(this.skills.skillMap.get(SKILL.maceProficiency).point));
        else if (this.handRight instanceof Axe)
            dmgR = (int)((double)dmgR * GetMasteryBonus(this.skills.skillMap.get(SKILL.axeProficiency).point));

        int dmg = dmgL + dmgR;

        dmg += this.STR;

        int precision = this.DEX;

        int result = r.nextInt((int)(dmg * (precision / 100.)) + 1 + 3*this.skills.skillMap.get(SKILL.weaponFocus).point) + dmg;

        // divide by zero is not checked for since, we are not able to equip two weapons without the skill
        if (this.handLeft != null && this.handRight != null)
            result -= 12 / this.skills.skillMap.get(SKILL.twoWeaponMastery).point;

        return result < 0 ? 0:result;
    }

    public void Attack(GameCharacter character) {
        int dmg = this.GenDmg();

        character.TakeDamage(this);
    }
}

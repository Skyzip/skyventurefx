package GameCharacters.Player;

public abstract class Player extends GameCharacter {
    private int nextXP = 1000;
    private int allXP = 0;

    protected void NextXP() {
        this.nextXP = (int)((int)(this.nextXP*1.1) > this.nextXP ? this.nextXP*1.1:this.nextXP+5);
    }

    protected void GainXP(int xp) {
        this.nextXP += xp;
    }
}
